import cats.effect._
import cats.implicits._
import dev.profunktor.redis4cats.Redis
import dev.profunktor.redis4cats.effect.Log.Stdout._

object QuickStart extends IOApp {

  override def run(args: List[String]): IO[ExitCode] =
    Redis[IO].utf8("redis://localhost").use { cmd =>
      for {
        _ <- cmd.set("foo", "123")
        x <- cmd.get("foo")
        _ <- cmd.setNx("foo", "should not happen")
        y <- cmd.get("foo")
        _ <- IO(println(x === y)) // true
      } yield ExitCode.Success
    }

}
