/*
import cats.effect.{ExitCode, IO, IOApp}
import log.effect.LogWriter
import log.effect.fs2.SyncLogWriter
import cats.syntax.flatMap._

object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    redisTest(SyncLogWriter.consoleLog[IO]).as(ExitCode.Success)
  }

  import laserdisc._
  import laserdisc.all._
  import laserdisc.auto._
  import laserdisc.fs2._

  def redisTest(implicit log: LogWriter[IO]): IO[Unit] =
    RedisClient.to("localhost", 6379).use { client =>
      client.send(
        set("a", 23),
        set("b", 55),
        get[PosInt]("b"),
        get[PosInt]("a")
      ) >>= {
        case (Right(OK), Right(OK), Right(Some(getOfb)), Right(Some(getOfa))) if getOfb.value == 55 && getOfa.value == 23 =>
          log info "yay!"
        case other =>
          log.error(s"something went terribly wrong $other") >>
            IO.raiseError(new RuntimeException("boom"))
      }
    }

}

 */