name := "test_redis4cats"

version := "0.1"

scalaVersion := "2.13.6"

//libraryDependencies += "io.laserdisc" %% "laserdisc-core" % "0.4.1"
//libraryDependencies += "io.laserdisc" %% "laserdisc-fs2" % "0.4.1"

libraryDependencies += "dev.profunktor" %% "redis4cats-effects" % "0.13.1"